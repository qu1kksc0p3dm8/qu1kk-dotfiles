PS1='\e[1;33m[\[\e[1;32m\u\] \e[1;35m\h \[\e[1;34m\W\]\[\e[1;33m\]\[\e[1m\]]\e[1;36m\$ \[\e[0m\]'

MANPAGER='cat -pp'
EDITOR='vim'

alias ls='ls --color'
alias grep='grep --color'
alias cat='cat -pp'
alias gen-search="equery list '*' | grep"
alias gen-use='equery uses'
alias wifi-list='nmcli dev wifi list'
alias wifi-connect='nmcli -a dev wifi connect'

alias emerge='doas emerge -v'
alias search='/usr/bin/emerge -s'
alias searchdesc='/usr/bin/emerge -S'
alias sync='emerge --sync'
alias update='emerge -auDN @world'
alias changed-use='emerge -aDU @world'
alias depclean='emerge -c'
alias unmerge='emerge -C'
alias upgrade='sync && update && depclean -a'

alias cd-use='cd /etc/portage/package.use'
alias cd-license='cd /etc/portage/package.license'
alias cd-config='cd /etc/portage/savedconfig'
alias cd-patches='cd /etc/portage/patches'
alias cd-themes='cd /usr/share/themes'
alias cd-icons='cd /usr/share/icons'
alias cd-fonts='cd /usr/share/fonts'
alias cd-portage='cd /etc/portage'

alias vim-make='doas vim /etc/portage/make.conf'
alias vim-dwm='doas vim /etc/portage/savedconfig/x11-wm/dwm-6.2.h'
alias vim-dmenu='doas vim /etc/portage/savedconfig/x11-misc/dmenu-5.0.h'
alias vim-st='doas vim /etc/portage/savedconfig/x11-terms/st-0.8.4.h'
alias vim-ufetch='vim ~/Code/ufetch-gentoo'

alias cat-make='cat /etc/portage/make.conf'
alias cat-dwm='cat /etc/portage/savedconfig/x11-wm/dwm-6.2.h'
alias cat-dmenu='cat /etc/portage/savedconfig/x11-misc/dmenu-5.0.h'
alias cat-world='cat /var/lib/portage/world'

alias rm-dwm='doas rm /etc/portage/savedconfig/x11-wm/dwm-6.2'
alias rm-dmenu='doas rm /etc/portage/savedconfig/x11-misc/dmenu-5.0.h'
alias rm-st='doas rm /etc/portage/savedconfig/x11-terms/st-0.8.4'

alias ufetch='~/Code/./ufetch-gentoo'
alias blender='blender-2.93'

alias youtube-vd='youtube-dl -f bestvideo[ext=mkv]+bestaudio[ext=wav]/bestvideo+bestaudio --merge-output-format mkv'
alias youtube-ad='youtube-dl -f bestaudio --extract-audio --audio-format=wav'

alias sudo='doas'
alias nano='vim'
